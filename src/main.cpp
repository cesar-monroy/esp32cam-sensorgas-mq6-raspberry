// Fecha: 1 de diciembre de 2021
// Autor: César Monroy
//
// Este programa realiza una lectura simple de un pin analogico en el ESP32CAM.
// Este programa está orientado a realizar la lectura del sensor MQ-6.
// ESP32CAM    MQ-6
// 3.3v        VCC
// GND         GND
// GPIO12      A0

// Bibliotecas
#include "Arduino.h" // Biblioteca para poder usar codigo de arduino

//Macros
#define sensor 12 // Macro para usar el pin 12

void setup() {
  Serial.begin(115200); // Inicia la comunicacion serial
}
void loop() {
  int dato = analogRead (sensor);//Lectura del sensor
  Serial.println(dato);//Se imprimen los datos
  delay (1000);//Espera estandar
}
