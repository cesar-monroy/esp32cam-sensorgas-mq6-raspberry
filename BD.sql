CREATE SCHEMA IF NOT EXISTS `sensorGas` ;
USE `sensorGas` ;
-- -----------------------------------------------------
-- Table `mydb`.`Datos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sensorGas`.`Datos` (
  `idDatos` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `concentracion` SMALLINT UNSIGNED NOT NULL,
  `time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idDatos`),
  UNIQUE INDEX `idDatos_UNIQUE` (`idDatos` ASC) VISIBLE)
ENGINE = MyISAM;
